# Figures

PhD stipend compared to minimum wage and living wage (calculated for a 37.5hr work week, with full tax and national insurance contributions). 

![graph of PhD stipend (mixed dates)](img/minwage_projection.png)

A second graph, with an equivalent wage for academic year (Sep-Aug) calculated from the overlap of neighbouring financial years (Apr-Mar).

![graph of PhD stipend (academic dates)](img/default.png)

A third graph, using "steps" plotting mode instead (easier to identify shortfall in PhD stipend wrt. equivalent minimum wage)

![graph of PhD stipend (steps)](img/minwage_steps.png)

# Data Sources


* PhD Stipend 1994-2012 <https://www.whatdotheyknow.com/request/phd_stipend_rates_for_the_last_1>
* PhD Stipend London 2015-2022 <https://www.ukri.org/what-we-offer/developing-people-and-skills/mrc/mrc-studentships/minimum-amounts-for-studentship-stipends-and-allowances>, <https://london-nerc-dtp.org/apply-to-the-london-nerc-dtp/resources-for-applicants> (use <https://web.archive.org>)
* PhD stipend London +£2000 dates back to at least 2006 <https://imechanica.org/node/746>
* Living wage 2003-2021 <https://www.livingwage.org.uk/calculation>
* Minimum wage 1999-2018 <https://www.gov.uk/government/publications/20-years-of-the-national-minimum-wage>
* Minimum wage 2016-2022 <https://www.gov.uk/national-minimum-wage-rates>
* Median wage 1997-2019 <https://www.ons.gov.uk/employmentandlabourmarket/peopleinwork/earningsandworkinghours/bulletins/annualsurveyofhoursandearnings/2019>
* Tax Calculator (tax bands and national insurance) <https://listentotaxman.com>
* Average VC 2019/20 <https://universitybusiness.co.uk/finance-legal-hr/average-vice-chancellor-salary-nears-270000>
* Percentiles income <https://www.gov.uk/government/statistics/percentile-points-from-1-to-99-for-total-income-before-and-after-tax>
* 2023 minimum wage projection (figure quoted on graph assumes a similar tax rate to 2022) <https://www.gov.uk/government/news/national-living-wage-increase-boosts-pay-of-low-paid-workers>
