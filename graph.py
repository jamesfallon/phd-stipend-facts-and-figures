import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


def plot_stipend(ax, dates):
    """UK minimum PhD stipend"""
    # read CSV data for PhD stipend
    stipend = pd.read_csv("stipend.csv", index_col="Academic Year")
    cols = ["Stipend (untaxed)", "London Stipend (untaxed)"]
    stipend.columns = cols

    # line style
    style = dict(color="dodgerblue", lw=3, drawstyle="steps-pre")

    # plot stipend data
    stipend.loc[dates][cols[0]].plot(ax=ax, ls="-", **style)
    stipend.loc[dates][cols[1]].plot(ax=ax, ls="--", **style)


def plot_minwage(ax, dates, cal="financial"):
    """UK Minimum Wage / Living Wage post tax"""
    # read CSV data for minimum/living wage
    minwage = pd.read_csv("minwage.csv", index_col="Financial Year")

    # using academic year means, or shift financial year dates to match?
    minwage.index.name = "Academic Year"
    if cal == "academic":
        # equivalent minimum wage for an academic year spans 7 months of current
        # financial year and 5 months of following academic year
        minwage = minwage[:-1] * 7 / 12 + (minwage[1:] * 5 / 12).values
    elif cal == "financial":
        # offset the minimumwage dates index to match academic year
        minwage.index = minwage.index - 5 / 12
        dates = slice(int(dates.start) - 1, int(dates.stop), dates.step)
    else:
        raise NotImplementedError

    # plot min/living wage data
    cols = ["Net Minimum Wage", "Net Living Wage", "Net London Living Wage"]
    color = "salmon"
    minwage[cols].loc[dates][cols[0]].plot(ax=ax, ls="-", color=color, drawstyle="steps-pre")

    # 2023 projected minimum wage
    if cal == "financial":
        projected_minwage = 17257.67
        ax.step(
            [2022 - 5 / 12, 2023 - 5 / 12],
            [minwage["Net Minimum Wage"].iloc[-1], projected_minwage],
            ls=":",
            label="Net Minimum Wage\n(gov.uk projection,\n2022/23 tax rate)",
            color=color,
            where="pre"
        )
        # equivalent minimum wage, but with NI reduced from 13.125% over
        # threshold to 12% over threshold
        # with no NI: 18615.00
        relief = 75.5625
        ax.step(
            [2022 - 5 / 12, 2023 - 5 / 12],
            [minwage["Net Minimum Wage"].iloc[-1], projected_minwage + relief],
            ls=":",
            label="Net Minimum Wage\n(gov.uk projection\n2022/23 & reduced NI)",
            color="tab:purple",
            where="pre"
        )

    color = "skyblue"
    minwage[cols].loc[dates][cols[1]].plot(ax=ax, color=color, ls="-", drawstyle="steps-pre")
    minwage[cols].loc[dates][cols[2]].plot(ax=ax, color=color, ls="--", drawstyle="steps-pre")


def plot_percentiles(ax, dates, cal="financial"):
    """Percentiles of UK income post tax"""
    # read taxed income percentiles
    percentiles = pd.read_csv("taxed_percentiles.csv", index_col=0).T
    percentiles.index = [int(y) for y in percentiles.index]

    if cal == "academic":
        # equivalent minimum wage for an academic year spans 7 months of current
        # financial year and 5 months of following academic year
        percentiles = percentiles[:-1] * 7 / 12 + (percentiles[1:] * 5 / 12).values
    elif cal == "financial":
        # offset the minimumwage dates index to match academic year
        percentiles.index = percentiles.index - 5 / 12
        dates = slice(int(dates.start) - 1, int(dates.stop), dates.step)
    else:
        raise NotImplementedError

    cmap = plt.get_cmap("RdBu")
    percentiles.loc[dates].plot(ax=ax, cmap=cmap, zorder=0, label=None)


def plot_main(include=(0, 1), dates=slice("2012", "2022"), cal="financial"):
    """Setup figure, call plotting subroutines

    Parameters
    ==========
    include (tuple[int]) : Plot elements to be included
    dates (slice) : Date range to plot
    cal (str) : Financial year data aggregatead or shifted to match academic?

    Notes
    =====
    Living wage introduced in 2012. By default, dates ignores earlier data
    """
    # load seaborn theme (comment out if seaborn not installed)
    sns.set_theme()

    # initialise plot
    fig, ax = plt.subplots(figsize=(8, 5))

    # xticks formatted in academic year (eg. 2020/21)
    ax.xaxis.set_major_formatter(lambda x, pos: f"{int(x)+1}/{int(x)-1998}")

    # yticks formatted in GBP (eg. £15,000)
    ax.yaxis.set_major_formatter(lambda y, pos: f"£{y:,.0f}")

    # plotting subroutines
    if 0 in include:
        plot_stipend(ax, dates)
    if 1 in include:
        plot_minwage(ax, dates, cal)
    if 2 in include:
        plot_percentiles(ax, dates, cal)

    # fun numbers
    if 3 in include:
        # Average VC salary
        data = 2019 - 5 / 12, 153606
        ax.scatter(*data, marker="*", color="g", label="Average VC (after tax)")
    if 4 in include:
        # Median income for 2021 financial year
        data = 2021 - 5 / 12, 24938
        ax.scatter(*data, marker="*", color="orange", label="UK Median Income")
    if 5 in include:
        # shortfall
        projected_minwage = 17257.67
        relief = 75.5625
        comp = projected_minwage + relief
        stipend = pd.read_csv("stipend.csv", index_col="Academic Year").loc[2022].Stipend
        ax.annotate("", xy=(2023, comp), xytext=(2023, stipend), arrowprops=dict(arrowstyle='<->', color="k"))

    ax.axvline(2021, c="k", ls="--", zorder=0)


    # legend (remove percentile labels if present)
    _h, _l = ax.get_legend_handles_labels()
    if "1" in _l:
        idx = _l.index("1")
        sidx = _l.index("99")
        _h, _l = _h[:idx] + _h[sidx:], _l[:idx] + _l[sidx:]
    ax.legend(_h, _l, loc="upper left", bbox_to_anchor=(1, 1), labelspacing=1.5)

    # xtick for each year, rotate labels
    ax.set_xticks(range(int(dates.start), int(dates.stop) + 1))
    fig.autofmt_xdate(rotation=60)

    # plot title
    fig.suptitle("PhD Stipend vs. Living Wage\n(37.5hr work week, after tax/NI)")

    # apply tight layout and return fig, ax
    fig.tight_layout()
    return fig, ax


if __name__ == "__main__":
    plot_main((0, 1, 5))
    plt.show()
